<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\PayOne\Security\Authentication\Provider;

use PIPEU\PayOne\Security\Authentication\Token\ApiKeyToken;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Security\Account;
use TYPO3\Flow\Security\Authentication\Provider\AbstractProvider;
use TYPO3\Flow\Security\Authentication\TokenInterface;
use PIPEU\Payment\Settings\Settings;
use PIPEU\Payment\Settings\Interfaces\SettingsInterface;
use TYPO3\Flow\Security\Exception\UnsupportedAuthenticationTokenException;
use TYPO3\Flow\Security\Policy\Role;
use TYPO3\Flow\Security\Context as SecurityContext;
use TYPO3\Flow\Utility\Arrays;

/**
 * Class ApiKeyProvider
 *
 * @package PIPEU\PayOne\Security\Authentication\Provider
 */
class ApiKeyProvider extends AbstractProvider {

	/**
	 * @var SecurityContext
	 * @Flow\Inject
	 */
	protected $securityContext;

	/**
	 * @var SettingsInterface
	 */
	protected $settings;

	/**
	 * @param array $settings
	 * @return void
	 */
	public function injectSettings(array $settings = array()) {
		$this->settings = new Settings($settings);
	}

	/**
	 * @return array
	 */
	public function getTokenClassNames() {
		return array('PIPEU\PayOne\Security\Authentication\Token\ApiKeyToken');
	}

	/**
	 * @param TokenInterface $authenticationToken
	 * @throws UnsupportedAuthenticationTokenException
	 */
	public function authenticate(TokenInterface $authenticationToken) {
		$credentials = $authenticationToken->getCredentials();
		$roleIdentifierSet = (array)Arrays::getValueByPath($this->options, 'authenticateRoles');
		if (!($authenticationToken instanceof ApiKeyToken)) {
			throw new UnsupportedAuthenticationTokenException('This provider cannot authenticate the given token.', 1414779627);
		} elseif ($credentials['apiKey'] === NULL) {
			$authenticationToken->setAuthenticationStatus(TokenInterface::NO_CREDENTIALS_GIVEN);
		} elseif ($credentials['apiKey'] === md5($this->settings->getValueByPath('security.apiKey'))) {
			$account = new Account();
			foreach ($roleIdentifierSet as $identifier) {
				$account->addRole(new Role($identifier, Role::SOURCE_SYSTEM));
			}
			$authenticationToken->setAccount($account);
			$authenticationToken->setAuthenticationStatus(TokenInterface::AUTHENTICATION_SUCCESSFUL);
		} else {
			$authenticationToken->setAuthenticationStatus(TokenInterface::WRONG_CREDENTIALS);
		}
	}
}
