<?php

/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\PayOne\Security\Authentication\Token;

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Security\Authentication\Token\AbstractToken;
use TYPO3\Flow\Security\Authentication\Token\SessionlessTokenInterface;
use TYPO3\Flow\Mvc\ActionRequest;

/**
 * Class ApiKeyToken
 *
 * @package PIPEU\PayOne\Security\Authentication\Token
 */
class ApiKeyToken extends AbstractToken implements SessionlessTokenInterface{

	/**
	 * The password credentials
	 *
	 * @var array
	 * @Flow\Transient
	 */
	protected $credentials = array('apiKey' => NULL);

	/**
	 * @var \TYPO3\Flow\Utility\Environment
	 * @Flow\Inject
	 */
	protected $environment;

	/**
	 * @param ActionRequest $actionRequest
	 * @return boolean
	 */
	public function updateCredentials(ActionRequest $actionRequest) {
		$apiKey = $actionRequest->getHttpRequest()->hasArgument('key') ? $actionRequest->getHttpRequest()->getArgument('key') : NULL;
		if ($apiKey !== NULL) {
			$this->credentials['apiKey'] = $apiKey;
			$this->setAuthenticationStatus(self::AUTHENTICATION_NEEDED);
		}
		return TRUE;
	}
}
