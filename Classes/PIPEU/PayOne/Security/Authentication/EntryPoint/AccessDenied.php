<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\PayOne\Security\Authentication\EntryPoint;

use TYPO3\Flow\Http\Request;
use TYPO3\Flow\Http\Response;
use TYPO3\Flow\Security\Authentication\EntryPoint\AbstractEntryPoint;
use TYPO3\Flow\Security\Exception\AuthenticationRequiredException;

/**
 * An authentication entry point, that sends an HTTP header to start HTTP Basic authentication.
 */
class AccessDenied extends AbstractEntryPoint {

	/**
	 * @param Request $request
	 * @param Response $response
	 * @throws AuthenticationRequiredException
	 */
	public function startAuthentication(Request $request, Response $response) {
		throw new AuthenticationRequiredException('Access Denied', 1414786101);
	}
}
