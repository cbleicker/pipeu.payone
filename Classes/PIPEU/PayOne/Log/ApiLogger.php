<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\PayOne\Log;

use TYPO3\Flow\Log\Logger as LoggerOrigin;

/**
 * Class ApiLogger
 *
 * @package PIPEU\PayOne\Log
 */
class ApiLogger extends LoggerOrigin {

	/**
	 * Writes information about the given exception into the log.
	 *
	 * @param \Exception $exception The exception to log
	 * @param array $additionalData Additional data to log
	 * @return void
	 */
	public function logException(\Exception $exception, array $additionalData = array()) {
		$backTrace = $exception->getTrace();
		$className = isset($backTrace[0]['class']) ? $backTrace[0]['class'] : '?';
		$methodName = isset($backTrace[0]['function']) ? $backTrace[0]['function'] : '?';
		$message = $this->getExceptionLogMessage($exception);

		if ($exception->getPrevious() !== NULL) {
			$additionalData['previousException'] = $this->getExceptionLogMessage($exception->getPrevious());
		}

		$explodedClassName = explode('\\', $className);
		// FIXME: This is not really the package key:
		$packageKey = (isset($explodedClassName[1])) ? $explodedClassName[1] : NULL;

		if (!file_exists(FLOW_PATH_DATA . 'Logs/PayOneExceptions')) {
			mkdir(FLOW_PATH_DATA . 'Logs/PayOneExceptions');
		}
		if (file_exists(FLOW_PATH_DATA . 'Logs/PayOneExceptions') && is_dir(FLOW_PATH_DATA . 'Logs/PayOneExceptions') && is_writable(FLOW_PATH_DATA . 'Logs/PayOneExceptions')) {
			$referenceCode = ($exception instanceof \TYPO3\Flow\Exception) ? $exception->getReferenceCode() : date('YmdHis', $_SERVER['REQUEST_TIME']) . substr(md5(rand()), 0, 6);
			$exceptionDumpPathAndFilename = FLOW_PATH_DATA . 'Logs/PayOneExceptions/' . $referenceCode . '.txt';
			file_put_contents($exceptionDumpPathAndFilename, $this->renderPostMortemInfo($message, $backTrace));
			$message .= ' - See also: ' . basename($exceptionDumpPathAndFilename);
		} else {
			$this->log(sprintf('Could not write exception backtrace into %s because the directory could not be created or is not writable.', FLOW_PATH_DATA . 'Logs/PayOneExceptions'), LOG_WARNING, array(), 'Flow', __CLASS__, __FUNCTION__);
		}

		$this->log($message, LOG_CRIT, $additionalData, $packageKey, $className, $methodName);
	}
}
