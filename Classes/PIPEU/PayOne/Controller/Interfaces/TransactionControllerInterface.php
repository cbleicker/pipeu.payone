<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\PayOne\Controller\Interfaces;

use TYPO3\Flow\Mvc\Controller\ControllerInterface;

/**
 * Interface TransactionControllerInterface
 *
 * @package PIPEU\PayOne\Controller\Interfaces
 */
interface TransactionControllerInterface extends ControllerInterface {

	const TRANSACTION_RESULT_PROPERTY_NAME = 'transactionResult', TRANSACTION_OK = 'TSOK';

}
