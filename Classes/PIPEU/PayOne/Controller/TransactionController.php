<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\PayOne\Controller;
use PIPEU\PayOne\Controller\Abstracts\AbstractTransactionController;

/**
 * Class TransactionController
 *
 * @package PIPEU\PayOne\Controller
 */
class TransactionController extends AbstractTransactionController {

	/**
	 * @return void
	 */
	public function appointedAction() {
		$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
	}

	/**
	 * @return void
	 */
	public function captureAction() {
		$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
	}

	/**
	 * @return void
	 */
	public function paidAction() {
		$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
	}

	/**
	 * @return void
	 */
	public function underpaidAction() {
		$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
	}

	/**
	 * @return void
	 */
	public function cancelationAction() {
		$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
	}

	/**
	 * @return void
	 */
	public function refundAction() {
		$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
	}

	/**
	 * @return void
	 */
	public function debitAction() {
		$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
	}

	/**
	 * @return void
	 */
	public function reminderAction() {
		$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
	}

	/**
	 * @return void
	 */
	public function vauthorizationAction() {
		$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
	}

	/**
	 * @return void
	 */
	public function vsettlementAction() {
		$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
	}

	/**
	 * @return void
	 */
	public function transferAction() {
		$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
	}

	/**
	 * @return void
	 */
	public function invoiceAction() {
		$this->view->assign(static::TRANSACTION_RESULT_PROPERTY_NAME, self::TRANSACTION_OK);
	}
}
