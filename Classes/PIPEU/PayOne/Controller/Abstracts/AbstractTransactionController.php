<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\PayOne\Controller\Abstracts;

use PIPEU\PayOne\Controller\Interfaces\TransactionControllerInterface;
use TYPO3\Flow\Mvc\Controller\ActionController;
use TYPO3\Flow\Annotations as Flow;
use PIPEU\PayOne\Log\Interfaces\TransactionLoggerInterface;

/**
 * Class AbstractTransactionController
 *
 * @package PIPEU\PayOne\Controller\Abstracts
 */
class AbstractTransactionController extends ActionController implements TransactionControllerInterface {

	/**
	 * @var TransactionLoggerInterface
	 * @Flow\Inject
	 */
	protected $transactionLogger;

	/**
	 * @return string
	 * @throws \Exception
	 */
	protected function resolveActionMethodName() {
		try {
			$controllerActionName = strtolower($this->request->getHttpRequest()->getArgument('txaction'));
			$this->request->setControllerActionName($controllerActionName);
			return parent::resolveActionMethodName();
		} catch (\Exception $exception) {
			throw $exception;
		} finally {
			$this->transactionLogger->log('Transaction Information', LOG_INFO, $this->request->getHttpRequest()->getArguments());
		}
	}

}
