<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\PayOne\Http;

use TYPO3\Flow\Error\Error;
use TYPO3\Flow\Error\Notice;
use TYPO3\Flow\Error\Result;
use TYPO3\Flow\Http\Response;

/**
 * Class ApiResult
 *
 * @package PIPEU\PayOne\Http
 */
class ApiResult extends Result {

	const STATUS_APPROVED = 'APPROVED', STATUS_INVALID = 'INVALID', STATUS_ERROR = 'ERROR', STATUS_REDIRECT = 'REDIRECT';

	/**
	 * @var array
	 */
	protected $data;

	/**
	 * @var string
	 */
	protected $status;

	/**
	 * @var string
	 */
	protected $redirectUri;

	/**
	 * @var integer
	 */
	protected $transactionId;

	/**
	 * @var integer
	 */
	protected $userId;

	/**
	 * @param Response $response
	 */
	public function __construct(Response $response) {
		$this->data = json_decode($response->getContent(), TRUE);
		$dataObject = new \ArrayObject((array)$this->data);
		if ($dataObject->offsetExists('status') && $dataObject->offsetGet('status') === static::STATUS_ERROR) {
			$this->status = static::STATUS_ERROR;
			$message = $dataObject->offsetExists('customermessage') ? $dataObject->offsetGet('customermessage') : 'Unkown Error';
			$error = new Error($message, 1414624984, array(), static::STATUS_ERROR);
			$this->addError($error);
		}
		if ($dataObject->offsetExists('status') && $dataObject->offsetGet('status') === static::STATUS_INVALID) {
			$this->status = static::STATUS_INVALID;
			$message = $dataObject->offsetExists('customermessage') ? $dataObject->offsetGet('customermessage') : 'Unkown Error';
			$error = new Error($message, 1414624985, array(), static::STATUS_INVALID);
			$this->addError($error);
		}
		if ($dataObject->offsetExists('status') && $dataObject->offsetGet('status') === static::STATUS_REDIRECT) {
			$this->status = static::STATUS_REDIRECT;
			$this->redirectUri = $dataObject->offsetGet('redirecturl');
		}
		if ($dataObject->offsetExists('status') && $dataObject->offsetGet('status') === static::STATUS_APPROVED) {
			$this->status = static::STATUS_APPROVED;
		}
		if ($dataObject->offsetExists('txid')) {
			$this->transactionId = $dataObject->offsetGet('txid');
		}
		if ($dataObject->offsetExists('userid')) {
			$this->userId = $dataObject->offsetGet('userid');
		}
	}

	/**
	 * @return string
	 */
	public function getRedirectUri() {
		return $this->redirectUri;
	}

	/**
	 * @return int
	 */
	public function getTransactionId() {
		return $this->transactionId;
	}

	/**
	 * @return int
	 */
	public function getUserId() {
		return $this->userId;
	}

	/**
	 * @return boolean
	 */
	public function getNeedsRedirect() {
		return $this->status === static::STATUS_REDIRECT;
	}
}
