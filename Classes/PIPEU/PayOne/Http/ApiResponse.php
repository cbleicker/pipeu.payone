<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\PayOne\Http;

use PIPEU\Payment\Http\ApiResponse as ApiResponseOrigin;
use TYPO3\Flow\Http\Response;
use TYPO3\Flow\Utility\Arrays;

/**
 * Class ApiResponse
 *
 * @package PIPEU\PayOne\Http
 */
class ApiResponse extends ApiResponseOrigin {

	/**
	 * @param Response $parentResponse
	 */
	public function __construct(Response $parentResponse = NULL) {
		parent::__construct($parentResponse);
		$this->headers->set('Content-Type', 'application/json; charset=' . $this->charset);
		if ($this->parentResponse !== NULL) {
			$this->setParsedContent();
		}
	}

	/**
	 * @return void
	 */
	protected function setParsedContent() {
		$parsedResult = new \ArrayObject();
		$splitByNewLine = Arrays::trimExplode("\n", (string)$this->parentResponse->getContent());
		foreach ($splitByNewLine as $line) {
			$propertyNameEndPos = strpos($line, '=');
			$propertyName = substr($line, 0, $propertyNameEndPos);
			$propertyValue = substr($line, $propertyNameEndPos + 1);
			$parsedResult->offsetSet($propertyName, $propertyValue);
		}
		$this->content = json_encode((array)$parsedResult);
	}
}
