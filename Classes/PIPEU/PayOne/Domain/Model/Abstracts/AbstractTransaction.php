<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\PayOne\Domain\Model\Abstracts;

use PIPEU\Payment\Domain\Model\Abstracts\AbstractTransaction as AbstractTransactionOrigin;
use PIPEU\Payment\Domain\Model\Interfaces\PaymentTypeInterface;
use PIPEU\Payment\Domain\Model\Interfaces\TransactionInterface;
use PIPEU\Payment\Domain\Service\Exceptions\PaymentTypeNotSupportedException;
use PIPEU\Payment\Settings\Settings;
use TYPO3\Flow\Utility\Arrays;

/**
 * Class AbstractTransaction
 *
 * @package PIPEU\PayOne\Domain\Model\Abstracts
 */
class AbstractTransaction extends AbstractTransactionOrigin {

	/**
	 * @param PaymentTypeInterface $paymentType
	 * @return static
	 */
	public static function create(PaymentTypeInterface $paymentType) {
		/** @var TransactionInterface $transaction */
		$transaction = new static();
		$defaultArguments = (array)$transaction->getSettings()->getValueByPath('request.defaultArguments');
		$paymentTypeArguments = (array)$transaction->getSettings()->getValueByPath('PaymentTypes.' . $paymentType->getType() . '.request.arguments');
		$transactionData = new Settings(Arrays::arrayMergeRecursiveOverrule($defaultArguments, $paymentTypeArguments));
		$transaction->exchangeArray($transactionData->getArrayCopy());
		return $transaction->ksort();
	}

	/**
	 * @param PaymentTypeInterface $paymentType
	 * @return string
	 * @throws PaymentTypeNotSupportedException
	 */
	public function getSimplePaymentType(PaymentTypeInterface $paymentType) {
		switch ($paymentType->getType()) {
			case 'PIPEU\Payment\Domain\Model\MasterCard':
				return 'M';
			case 'PIPEU\Payment\Domain\Model\DinersCard':
				return 'D';
			case 'PIPEU\Payment\Domain\Model\VisaCard':
				return 'V';
		}
		throw new PaymentTypeNotSupportedException('This payment type is not supported', 1414520543);
	}

	/**
	 * @param TransactionInterface $transaction
	 * @return string
	 */
	public static function getHash(TransactionInterface $transaction) {
		$hashValues = array();
		$argumentsNeedsToBeHashed = (array)$transaction->getSettings()->getValueByPath('security.argumentsNeedsToBeHashed');
		$iterator = $transaction->getIterator();
		$iterator->rewind();
		$iterator->ksort();
		while ($iterator->valid()) {
			if (in_array($iterator->key(), $argumentsNeedsToBeHashed)) {
				$hashValues[] = $iterator->current();
			}
			$iterator->next();
		}
		return md5(implode('', $hashValues) . $transaction->getSettings()->getValueByPath('security.apiKey'));
	}
}
