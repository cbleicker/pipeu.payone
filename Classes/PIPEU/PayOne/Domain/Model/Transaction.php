<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\PayOne\Domain\Model;

use PIPEU\PayOne\Domain\Model\Abstracts\AbstractTransaction;

/**
 * Class Transaction
 *
 * @package PIPEU\PayOne\Domain\Model
 */
class Transaction extends AbstractTransaction {

}
